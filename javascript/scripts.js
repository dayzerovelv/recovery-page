var urlString = window.location.href;
var url = new URL(urlString);
var token = url.searchParams.get("token");
var baseUrl = 'http://dev.contab.velv.tech:8089/my-feedback/api';
var errorMessage = document.getElementsByClassName('form-error')[0];
var loading = document.getElementsByClassName('loading')[0];


function sendNewPassword() {
    var newPassword = document.getElementById('newPassword').value,
        confirmNewPassword = document.getElementById('confirmNewPassword').value,
        apiUrl = baseUrl + 'recoverPassword';

    if(validatePassword(newPassword, confirmNewPassword)){
        errorMessage.classList.remove('has-error');
        loading.classList.add('is-loading');

        setTimeout(function(){
            loading.classList.remove('is-loading');
        }, 2000)
      /*   $.ajax(`${apiUrl}/${token}`, {
            method: 'POST',
            data: {
                newPassword: newPassword,
                confirmNewPassword: confirmNewPassword
            }
        })
        .then(
            function success(resp) {
                loading.classList.remove('is-loading');
                console.log(resp);
                
            },
            function fail(data, status) {
                loading.classList.remove('is-loading');
                console.log('Request failed.  Returned status of ' + status);
            }
        ); */
    } else {
        errorMessage.classList.add('has-error');
    }

}

function clearErrors() {
    errorMessage.classList.remove('has-error');
}

function validatePassword(newPassword, confirmNewPassword) {
    var isValid = false;

    if(newPassword && confirmNewPassword) {
        if((newPassword.length >= 8 && confirmNewPassword.length >= 8) && (newPassword == confirmNewPassword)) {
            isValid = true;
        } 
    }

    return isValid;
}